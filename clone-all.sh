#!/bin/bash

# Clone all services main branch
git clone https://gitlab.com/pet-vadim/service-tester
git clone https://gitlab.com/pet-vadim/service-mail
git clone https://gitlab.com/pet-vadim/service-distributor
git clone https://gitlab.com/pet-vadim/service-dashboard
git clone https://gitlab.com/pet-vadim/service-auth
git clone https://gitlab.com/pet-vadim/migrator.git