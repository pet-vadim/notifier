# Notifier

Сервис для создания тестов, которые будут выполняться в фоне с определенным интервалом, в случе ошибки пользователю
придет уведомление на его email или в телеграм

## Используемые технологии

Gin, JWT, PG, Redis, Swagger, K8s, Helm, Terraform, RabbitMQ

## Репозитории сервисов и общих библиотек
[service-auth](https://gitlab.com/pet-vadim/service-auth)  
[service-dashboard](https://gitlab.com/pet-vadim/service-dashboard)  
[service-distributor](https://gitlab.com/pet-vadim/service-distributor)  
[service-tester](https://gitlab.com/pet-vadim/service-tester)  
[service-mail](https://gitlab.com/pet-vadim/service-mail)  
[migrator](https://gitlab.com/pet-vadim/migrator)  
[libs](https://gitlab.com/pet-vadim/libs)

## Архитектура сервиса

Сервис написан таким образом, чтобы можно было разделить нагрузку на разные части программы. Пользовательский REST для
авторизации и для создания тестов вынесены в отдельные сервисы, чтобы нагрузка во время выполнения самих тест кейсов не
влияла на скорость отклика для пользователя.

- Сервис Distributor использует курсоры для того, чтобы выбрать поочередно с правильным интервалом все тесты и отправить
их в очередь. С него максимально снята вся нагрузка потому как его нельзя масштабировать горизонтально

- Сервис Tester выполняет сами тест кейсы и может быть легко масштабирован до необходимого количества реплик

- Telegram && Mail сервисы сделаны максимально атомарными, чтобы их было легко переиспользовать

![image info](./Diagram.drawio.png)

## Локальное развертывание

Для локального запуска:

1) Проверить установку необходимых приложений
    - docker
    - minikube
    - kubectl
    - helm
    - python 3

2) Освободить порт 8080  

3) Cклонировать сервисы  
    ```make clone```
4) Запуск minikube  
   ```minikube start```
5) Разрешение minikube на ingress  
   ```minikube addons enable ingress```
6) Создание сервисов (В браузере откроются окна swagger, но необходимо будет дождаться запуска всех сервисов)  
   ```make create```
7) удаление всех сервисов  
   ```make delete```
   
* Используются тестовые credentials для docker и в качестве переменных для сервисов


## TODO

- Добавить request id ри каждом запросе в context
- Добавить возможных тест кейсов
- Дописать Telegram notifier
- Индексы в PG
- Переделать деплой на helm
- Установить ограничение на ресурсы каждому контейнеру
- Сделать Gitlab CI/CD
