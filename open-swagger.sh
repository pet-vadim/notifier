#!/bin/bash

python3 -m webbrowser "http://localhost:8080/authentication/swagger/index.html#/"
python3 -m webbrowser "http://localhost:8080/dashboard/swagger/index.html#/"

# This maps port 80 in minikube's cluster to 8080 in localhost.
ssh -i "$(minikube ssh-key)" docker@"$(minikube ip)" -L 8080:localhost:80
