clone:
	./clone-all.sh

create:
	./minikube-cluster.sh create
	./open-swagger.sh

delete:
	./minikube-cluster.sh delete