#!/bin/bash

if [ "$1" != "create" ] && [ "$1" != "delete" ]; then
  echo "argument should be: create or delete"
  exit 1
fi

# CREATE
if [ "$1" == "create" ]; then
  # docker credential for test account
  kubectl apply -f ./k8s/docker-secret/docker-secret.yaml

  # secretes for services and migrator
  kubectl create secret generic auth-secrets --from-env-file=./service-auth/.env
  kubectl create secret generic dashboard-secrets --from-env-file=./service-dashboard/.env
  kubectl create secret generic distributor-secrets --from-env-file=./service-distributor/.env
  kubectl create secret generic mail-secrets --from-env-file=./service-mail/.env
  kubectl create secret generic tester-secrets --from-env-file=./service-tester/.env
  kubectl create secret generic migrator-secrets --from-env-file=./migrator/.env

  # postgres
  kubectl apply -f ./k8s/postgres/postgres-db-pv.yaml \
    -f ./k8s/postgres/postgres-db-pvc.yaml \
    -f ./k8s/postgres/postgres-db-secret.yaml \
    -f ./k8s/postgres/postgres-db-deployment.yaml \
    -f ./k8s/postgres/postgres-db-service.yaml

  # migrator for postgres
  kubectl apply -f ./k8s/migrator/migrator-pod.yaml

  # redis
  kubectl apply -f ./k8s/redis/redis-deployment.yaml
  kubectl apply -f ./k8s/redis/redis-service.yaml

  # rabbit mq
  helm repo add bitnami https://charts.bitnami.com/bitnami
  helm install -f ./k8s/rabbitmq/values.yaml rmq bitnami/rabbitmq


  # services
  kubectl apply -f ./k8s/auth-deployment.yaml \
    -f ./k8s/dashboard-deployment.yaml \
    -f ./k8s/distributor-deployment.yaml \
    -f ./k8s/mail-deployment.yaml \
    -f ./k8s/tester-deployment.yaml
fi

# DELETE
if [ "$1" == "delete" ]; then
  # docker credential for test account
  kubectl delete -f ./k8s/docker-secret/docker-secret.yaml

  # postgres
  kubectl delete -f ./k8s/postgres/postgres-db-secret.yaml \
    -f ./k8s/postgres/postgres-db-deployment.yaml \
    -f ./k8s/postgres/postgres-db-service.yaml \
    -f ./k8s/postgres/postgres-db-pvc.yaml \
    -f ./k8s/postgres/postgres-db-pv.yaml

  # redis
  kubectl delete -f ./k8s/redis/redis-deployment.yaml \
    -f ./k8s/redis/redis-service.yaml

  # rabbit mq
  helm repo add bitnami https://charts.bitnami.com/bitnami
  helm delete rmq

  # secrets for services
  kubectl delete secrets \
    auth-secrets \
    dashboard-secrets \
    distributor-secrets \
    mail-secrets \
    tester-secrets \
    migrator-secrets

  # services
  kubectl delete -f ./k8s/auth-deployment.yaml \
    -f ./k8s/dashboard-deployment.yaml \
    -f ./k8s/distributor-deployment.yaml \
    -f ./k8s/mail-deployment.yaml \
    -f ./k8s/tester-deployment.yaml

fi
